"""
Convenience script to deactivate all Auto ADCP related tasks in Task Scheduler. If the 
REENABLE flag ist True the Tasks will be reenabled immediately, resulting in normal operation
after next logon of user.

Author: thomas.kock@hereon.de
Date: 2024-01-04
"""

import configparser
import pathlib
import win32com.client
import time
import logging, sys, datetime

REENABLE: bool = True
STOP_RUNNING: bool = True
TASK_STATE_RUNNING: int = 4

logger = logging.getLogger("ts_disable")
log_path = pathlib.Path().home() / r"Documents\log_auto_adcp"

# set up the logger
_fmt = logging.Formatter("{levelname:9} {asctime:24} {name} >>> {message}", style="{")
logger.setLevel(logging.DEBUG)
_stream_handler = logging.StreamHandler(sys.stdout)
_stream_handler.setLevel(logging.INFO)
_stream_handler.setFormatter(_fmt)
logger.addHandler(_stream_handler)

_log_file = log_path / f"auto_adcp_{datetime.datetime.now().strftime('%Y%m%dT%H%M%S')}_disable_tasks.log"
_file_handler = logging.FileHandler(_log_file)
_file_handler.setLevel(logging.DEBUG)
_file_handler.setFormatter(_fmt)
logger.addHandler(_file_handler)
logger.info(f"Logging to {_log_file}")

logger.debug(f"Flag REENABLE={REENABLE}")
logger.debug(f"Flag STOP_RUNNING={STOP_RUNNING}")

cfg_path = pathlib.Path(__file__).resolve().parent / "setup_tasks.ini"
logger.debug(f"Reading config from {cfg_path}")
cfg = configparser.ConfigParser()
cfg.read(cfg_path)
task_folder = "\\Auto ADCP"
logger.debug(f"Using task folder {task_folder}")

for sec in cfg.values():
    
    if sec.name == "DEFAULT":
        logger.debug(f"Skipped default .ini section")
        continue

    scheduler = win32com.client.Dispatch("Schedule.Service")
    scheduler.Connect()

    task_folder = scheduler.GetFolder(task_folder)
    task_def = task_folder.GetTask(sec["task_name"])
    logger.debug(f"Current task: {task_def.Path}")
    
    if STOP_RUNNING and task_def.State == TASK_STATE_RUNNING:
        logger.debug(f"Task {task_def.Name} is running. Stopping it now.")
        task_def.Stop(0)
    
    task_def.Enabled = False
    logger.debug(f"Disabling task {task_def.Name}")

    if REENABLE:
        time.sleep(0.1)
        task_def.Enabled = True
        logger.debug(f"Reenabled task {task_def.Name}")

print("\n\nTasks disabled until next logon.")
print("\n\nNote that disabling tasks does not close any running instances of Winriver 2\n\n")
for i in range(60,0,-1):
    print(f"\rThis window closes in {i:>2} s", end="")
    sys.stdout.flush()
    time.sleep(1)