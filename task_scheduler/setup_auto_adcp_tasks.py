"""
A convencience script to set all required tasks for auto adcp with the settings set in
setup_tasks.ini file.

Author: thomas.kock@hereon.de
Date: 2024-01-03
"""

import configparser
import pathlib

from lib_taskscheduler import create_or_update_task

if __name__ == "__main__":

    __cfg__ = configparser.ConfigParser()
    __cfg__.read(pathlib.Path(__file__).resolve().parent / "setup_tasks.ini")

    task_folder = "Auto ADCP"

    for sec in __cfg__.values():
        if sec.name == "DEFAULT":
            continue
        task = create_or_update_task(sec, task_folder=task_folder)
        print(f"Created task: '{sec['task_name']}' in task folder {task_folder}")
   
