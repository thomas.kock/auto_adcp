"""
A convencience script to remove tasks for auto adcp from Task Scheduler folder Auto ADCP

Author: thomas.kock@hereon.de
Date: 2024-01-03
"""

from lib_taskscheduler import delete_task_folder

if __name__ == "__main__":
    
    delete_task_folder("Auto ADCP")