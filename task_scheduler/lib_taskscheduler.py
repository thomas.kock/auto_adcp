"""
An interface to the Windows OS task scheduler system for creating, updating and deleting tasks.
The functionaliy of this module is rather barebones and build around a specific requirement.
It does not handle any egde cases nor is it tested against other use cases, it does however
do what I needed at the time of creation.

The class WindowsTask is the main interface to interact with the desired task. It allows to 
create and or update a task, set properties and register it with the task scheduler. It 
should be possible to also deactivate tasks using the interface, see TASK_TRIGGER below, but
this is untested.

Using the delete function of this module it is possible to either delete a single task
or delete a complete tasks directory. Use the latter version with cause as it has the 
power to break your Windows setup!

As this is targeted at Windows Systems there is no need for platform independency. For instance
will os.environ lookup fail on non Windows machines.

All information about the task scheduler interfac is either from 

https://learn.microsoft.com/en-us/windows/win32/taskschd/task-scheduler-reference [2023-11-13]

or from the respective C libraries, like the constants in the enums.

Author: Thomas Kock
E-Mail: thomas.kock@hereon.de
Date: 2024-01-03
"""

import configparser
import enum
import pathlib
import win32com.client
import os

# the following symbolic constant are required for task creation and are environment
# dependent. USERDOMAIN default to COMPUTERNAME if no domain account is used
__USERNAME__ = os.environ["USERNAME"]
__USERDOMAIN__ = os.environ["USERDOMAIN"]

# these are to error codes I encountered repeatedly during development. They are not from
# any docs and perhaps not as constant as I suppose.
EXCEPTION_TASK_FOLDER_EXISTS = -2147024713
EXCEPTION_CANNOT_GET_TASK_FOLDER = -2147024894

# these enums are copies from the respective C libraries.
class TASK_TRIGGER1(enum.IntEnum):
    TIME_ONCE = 0
    TIME_DAILY = 1
    TIME_WEEKLY = 2
    TIME_MONTHLYDATE = 3
    TIME_MONTHLYDOW = 4
    EVENT_ON_IDLE = 5
    EVENT_AT_SYSTEMSTART = 6
    EVENT_AT_LOGON = 7

class TASK_TRIGGER2(enum.IntEnum):
    EVENT = 0
    TIME = 1
    DAILY = 2
    WEEKLY = 3
    MONTHLY = 4
    MONTHLYDOW = 5
    IDLE = 6
    REGISTRATION = 7
    BOOT = 8
    LOGON = 9
    SESSION_STATE_CHANGE = 11
    CUSTOM_TRIGGER_01 = 12

class TASK_ACTION(enum.IntEnum):
    EXEC = 0,
    COM_HANDLER = 5,
    SEND_EMAIL = 6,
    SHOW_MESSAGE = 7

class TASK_CREATION(enum.IntEnum):
    VALIDATE_ONLY = 0x1
    CREATE = 0x2
    UPDATE = 0x4
    CREATE_OR_UPDATE = 0x6
    DISABLE = 0x8
    DONT_ADD_PRINCIPAL_ACE = 0x10
    IGNORE_REGISTRATION_TRIGGERS = 0x20

class TASK_LOGON_TYPE(enum.IntEnum):
    NONE = 0
    PASSWORD = 1
    S4U = 2
    INTERACTIVE_TOKEN = 3
    GROUP = 4
    SERVICE_ACCOUNT = 5
    INTERACTIVE_TOKEN_OR_PASSWORD = 6

class WindowsTask():

    def __init__(self, task_folder: str = ""):
        """
        WindowsTask is can be used to create, setup and register a task using Windows
        task scheduler. This is a rather barebones version somewhat tailormade for 
        my work project auto_adcp.

        Args:
            root_folder (str, optional): _description_. Defaults to "\".
        """
        self.scheduler = win32com.client.Dispatch('Schedule.Service')
        self.scheduler.Connect()
        root_folder = self.scheduler.GetFolder("\\")
        if task_folder:
            try: 
                root_folder.CreateFolder(f"\\{task_folder}")
            except Exception as e:
                if e.excepinfo[-1] == EXCEPTION_TASK_FOLDER_EXISTS:
                    pass
                else:
                    raise e
            
            try:
                self.task_folder = root_folder.GetFolder(f"\\{task_folder}")
            except Exception as e:
                if e.excepinfo[-1] == EXCEPTION_CANNOT_GET_TASK_FOLDER:
                    print(f"Cannot get task folder {task_folder}")
                    raise e
                else:
                    print(f"Unkown error while trying to get task folder {task_folder}")
                    raise e
        else:
            self.task_folder = root_folder

        self.task = self.scheduler.NewTask(0)
        self.is_defined: bool = False
        self.name = None
        self.action = None
        self.trigger = None
    
    def __str__(self) -> str:
        return "Windows Task instance via pywin32"
    
    def __repr__(self) -> str:
        return f"Windows Task at 0x{id(self):0X} in task folder {self.task_folder}"
    
    def get_task_definition(self):
        if self.is_defined:
            return self.task_folder.GetTask(self.name)
        
    def get_task_enabled(self) -> bool:
        if self.is_defined:
            task_def = self.get_task_definition()
            return task_def.Enabled
    
    def set_task_enabled(self, flag: bool = True) -> None:
        if self.is_defined:
            task_def = self.get_task_definition()
            task_def.Enabled = flag
    
    def create_trigger(self, trigger_type: int = TASK_TRIGGER2.TIME) -> None:
        """
        Creates and adds a trigger to the current Windows Task instance

        Args:
            trigger_type (int, optional): Type of trigger as defined in the TASK_TRIGGER2 enum. 
            Defaults to TASK_TRIGGER2.TIME.
        """
        self.trigger = self.task.Triggers.Create(trigger_type)
    
    def create_action(
            self, 
            action_type: int, 
            path: str | pathlib.Path,
            label: str = "Action label",
            args: str = "") -> None:
        """
        Creates an action for the current Windows Task instance.

        The executable passed with path can accept arguments using the args variable. Both 
        combind should look somehting like this:
            C:\path\to\executable.exe -arg1 --long_arg1 ...
            ^path                     ^args

        Args:
            action_type (int): The action type is defined in the TASK_ACTION enum. In 2024
            only 0=exec seems to be in use. The other options are marked as obsolete.
            path (str | pathlib.Path): A path to an executable program
            label (str, optional): A label for the action. Defaults to "Action label".
            args (str, optional): Arguments for the executable program. Defaults to "".
        """
        self.action = self.task.Actions.Create(action_type)
        self.action.ID = label
        self.action.Path = path
        self.action.Arguments = args
    
    def set_parameter(self, obj: str , prop: str, val: any) -> None:
        """
        Helper function to set properties of some instance object. This method is kept
        very general to allow settings almost all properties using a single function.

        Example usage to set the property "Repitition.Interval of object trigger:
             task.set_parameter("trigger", "Repetition.Interval", "PT5M")

        Args:
            obj (str): The object of task self to set, for instance 'trigger'
            prop (str): The property of obj to set, refer to MSDN docs for possible properties
            val (any): The value, usually int or str.
        """

        base_obj = getattr(self, obj)
        sub_obj = prop.split(".")
        if len(sub_obj) == 1:
            p = sub_obj[0]
        else:
            for so in sub_obj[:-1]:
                base_obj = getattr(base_obj, so)
            p = sub_obj[-1]
        setattr(base_obj, p, val)
    
    def register_task_in_task_scheduler(
            self, 
            task_name: str = "MyTask", 
            task_creation: int = TASK_CREATION.CREATE_OR_UPDATE,
            task_logon_type: int = TASK_LOGON_TYPE.NONE,
            user: str = "",
            passwd: str = ""
            ) -> int:
        """
        Registers the task within the Windows Task Scheduler. This is only a wrapper around
        WindowsTask.TaskFolder.RegisterTastDefinition()

        Note that the parameters user and passwd are available only for completeness but cannot be
        used.

        Args:
            task_name (str, optional): This name will be listed in the Task Scheduler library. Defaults to "MyTask".
            task_creation (int, optional): The creation mode. Defaults to TASK_CREATION.CREATE_OR_UPDATE.
            task_logon_type (int, optional): Logon Type. Defaults to TASK_LOGON_TYPE.NONE.
            user (str, optional): Specific user name, not used here. Defaults to "".
            passwd (str, optional): Password of that user, not used here. Defaults to "".

        Returns:
            int: 0 if successful
        """
        self.task_folder.RegisterTaskDefinition(
            task_name,
            self.task,
            task_creation,
            user,
            passwd,
            task_logon_type)
        self.is_defined = True
        self.name = task_name
        return 0

def create_or_update_task(
        ini_sect: configparser.SectionProxy,
        task_folder: str = "",
        task_action: int = TASK_ACTION.EXEC
        ) -> WindowsTask:
    """
    Function to create a specific task. This function is tailormade for Auto ADCP use and
    requires a config.ini file. Each section in this file describes one task, thus this 
    function needs a single section of this file.

    Args:
        ini_sect (configparser.SectionProxy): Section of ini file describing one task.
        task_folder (str, optional): The task folder to place the task in.
        The default uses the root folder. Defaults to "".
        task_action (int, optional): The action type. This is set but not functional. 
        Defaults to TASK_ACTION.EXEC.

    Returns:
        WindowsTask: A pywin32 instance of a WindowsTask
    """
    task = WindowsTask(task_folder=task_folder)

    if (tc := int(ini_sect["trigger_class"])) not in [1, 2]:
        raise ValueError(f"Trigger class must be either 1 or 2, got {tc}")

    tt = int(ini_sect["trigger_type"])
    if tc == 1 and tt not in iter(TASK_TRIGGER1):
        raise ValueError(f"Task trigger type {tt} is not allowed in trigger class 1")
    elif tc == 2 and tt not in iter(TASK_TRIGGER2):
        raise ValueError(f"Task trigger type {tt} is not allowed in trigger class 2")

    task.create_trigger(trigger_type=tt)
    task.set_parameter("trigger", "UserId", f"{__USERDOMAIN__}\\{__USERNAME__}")

    if (trigger_repitition := ini_sect["trigger_repitition"]) != "None":
        task.set_parameter("trigger", "Repetition.Interval", trigger_repitition)

    if (trigger_delay := ini_sect["trigger_delay"]) != "None":
        task.set_parameter("trigger", "Delay", trigger_delay)
    
    task.create_action(
        task_action, 
        ini_sect["action_run"], 
        label = ini_sect["action_label"],
        args = ini_sect["action_args"]
        )
    task.set_parameter(
        "action", 
        "WorkingDirectory", 
        ini_sect["action_in"]
        )

    task.set_parameter(
        "task", 
        "RegistrationInfo.Description", 
        ini_sect["task_registration_info"]
        )
    # these two are hardcoded for this purpose
    task.set_parameter("task", "Settings.Enabled", True)
    task.set_parameter("task", "Settings.StopIfGoingOnBatteries", False)

    # finally the task is registered with the Windows Task Scheduler and given a name.
    task.register_task_in_task_scheduler(
        task_name = ini_sect["task_name"]
        )
    
    return task

def delete_task(
        task_name: str,
        task_folder: str = "\\", 
        ) -> int:
    """
    Function to delete a single task from Task Scheduler library

    Args:
        task_name (str): Name of the task to delete.
        task_folder (str, optional): Folder where the task is registered. Defaults to "\".

    Returns:
        int: 0 if successful, -1 if task_name does not exist
    """
    
    scheduler = win32com.client.Dispatch("Schedule.Service")
    scheduler.Connect()
    try:
        tasks_folder = scheduler.GetFolder(task_folder)
    except Exception as e:
        if e.excepinfo[-1] == EXCEPTION_CANNOT_GET_TASK_FOLDER:
            print(f"Cannot get task folder {task_folder}. Does it exist?")
        else:
            print(f"Unknown except while trying to get task folder {task_folder}")
            raise e
    all_task_names = [_.Name for _ in tasks_folder.GetTasks(0)]
    if task_name not in all_task_names:
        return -1
    else:
        tasks_folder.DeleteTask(task_name, 0)
        return 0

def delete_task_folder(task_folder: str = "Auto ADCP") -> int:
    """
    Convenience function to clean up an entire tasks folder. Be careful, you should not pass 
    the root folder as task_folder because it can result in deleting ALL tasks!

    Args:
        task_folder (str, optional): The tasks_folder to delete from Task Schedulers
        library. Defaults to "Auto ADCP".

    Returns:
        int: 0 on normal exit, -1 if the task folder cannot be found
    """

    print(f"WARNING: All tasks from folder {task_folder} will be DELETED!")
    c = input("Is that correct: [y]es or [n]o <<< ")
    if c.casefold() != "y":
        print("Aborting")
        return 0
    
    scheduler = win32com.client.Dispatch("Schedule.Service")
    scheduler.Connect()

    parent_folder = "\\"
    folder_path_parts = task_folder.split(os.path.sep)
    if len(folder_path_parts) > 1:
        for p in folder_path_parts[:-1]:
            parent_folder = parent_folder + p + "\\"
        parent_folder = parent_folder[:-2]

    try:
        target_tasks_folder = scheduler.GetFolder(task_folder)
    except Exception as e:
        if e.excepinfo[-1] == EXCEPTION_CANNOT_GET_TASK_FOLDER:
            print(f"Cannot get task folder {task_folder}. Does it exist?")
            return -1
        else:
            print(f"Unknown except while trying to get task folder {task_folder}")
            raise e
    
    all_task_names = [_.Name for _ in target_tasks_folder.GetTasks(0)]
    # before fodler can be deleted all tasks must be deleted
    for task_name in all_task_names:
        target_tasks_folder.DeleteTask(task_name, 0)
        print(f"Deleted task: '{task_name}' from folder {task_folder}")
    
    # get the parent folder instance and delete desired folder from there
    if len(list(target_tasks_folder.GetTasks(0))) == 0:
        rf = scheduler.GetFolder(parent_folder)
        rf.DeleteFolder(task_folder, 0)
        print(f"Deleted empty tasks folder {parent_folder}{os.path.sep}{task_folder}")
    
    return 0
    