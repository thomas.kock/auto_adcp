# Auto ADCP

Auto ADCP is a collection of automation tools for Teledyne RDIs WinRiver2 (WR2) ADCP data 
acquisition software. It has been developed to enable "turnkey" operation after PC power on, 
much like built-in feature in WinRiver1. Auto ADCP has been developed at 
Helmholtz-Zentrum Hereon for use on Hereons Research Vessel Ludwig Prandtl. It is somewhat 
tailor made for this setup, but may be modified and adapted to other platforms. Auto ADCP does 
not provide an installer, settings are made in many different places, see detailed setup 
information [in the docs of this repo](./doc/_build/html/index.html)

* Contact: thomas.kock@hereon.de
* Date of this readme: 2024-01-08
* License: 3-Clause BSD

## What Auto ADCP can do
As stated above Auto ADCP's main function is to enable unsupervised turnkey water current data
collection by adding automation to TRDIs Winriver2 software package. To do so Auto ADCP offers 
scripts that can be placed in Windows as tasks using the Task scheduler, see 
[detailed set up instructions](./doc/_build/html/index.html)

Auto ADCP has also the ability to copy collected datasets into other directories for 
near-real-time (NRT) data transfer.

A further option allows to parse the binary raw data files and execute various processing steps 
on the data for further use. This functionality relies on another python libary called __pydcp2__
which is __not__ shipped with Auto ADCP.

Auto ADCP can supervise the memory usage of Winriver 2 and if desired restart data acquisistion
after the working set size in memory exceeds a certain threshold. This feature circumvents a 
memory leak present in Winriver 2 and allows for continuous data collection over many hours or
days, which would be impossible without.

## Requirements
To set up Auto ADCP successfully meet these requirements

* Windows 10 Pro 22H2 or later
* Winriver 2.16 (exactly this version)
* python 3.11 with additional packages pywin32, pyserial

## Copyright holder

Helmholtz-Zentrum Hereon GmbH  
Max-Planck-Straße 1  
21502 Geesthacht  
Germany
