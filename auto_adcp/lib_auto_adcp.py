"""
| auto_adcp libary
| Author: Thomas Kock
| E-Mail: thomas.kock@hereon.de
| Copyright holder: Helmholtz-Zentrum Hereon GmbH
| Date: 2023-07-07
"""
import logging
logger = logging.getLogger("auto_adcp.lib_auto_adcp")
import datetime, glob, os, shutil, time, subprocess
import pathlib
import hashlib
import serial
import sys

try:
    import win32gui, win32com.client, win32con, win32process, win32api
except ImportError:
    logger.error("Cannot import win32 modules.")
    raise ImportError

try:
    import pydcp2.trdi_codec as trdi
    import pydcp2.io
    import pydcp2.processing
    import pydcp2.geo
    import pydcp2.tools
    PYDCP2 = True
except ImportError:
    logger.warning("Cannot import pydcp2, some functionality will not be available")
    PYDCP2 = False

from auto_adcp import wr2gui
from time import perf_counter

__version_major__ = 0
__version_minor__ = 3

# some keybindings
VK_F4 = "{F4}"
VK_F5 = "{F5}"
VK_TAB = "{TAB}"
VK_ENTER = "~"
VK_UP = "{UP}"
VK_DOWN = "{DOWN}"
VK_SPACE = " "
VK_ESC = "{ESC}"
VK_ALT = "%"
VK_CTRL = "^"
VK_SHIFT = "+"



class SimpleWindowManager():
    """
    Wrapper class around some methods for pywin32 module win32gui. Because WinRiver 2 has no
    automation interface like COM we rely on sending keystrokes to focused windows.
    
    This is most likely not at all perfect, but there seems to be no other way. If you find a
    better way please report back to thomas.kock@hereon.de
    """

    def __init__(self, appname: str = "", hwnd: int = 0):
        """
        Init method of WindowManager.

        Args:
            appname (str, optional): If given, try to launch this app. Defaults to "".
            hwnd (int, optional): If given this can be used as a shortcut. Defaults to 0.
        """
        self.hwnd = hwnd
        self.appname = appname
        self.window_text = None
        self.app_launched = False
        self.app_launch_code = -99

    def launch_app(self, wait_after: float = 1):
        """
        If self has an appname this method can be used to launch the app. Method relies on
        module function get_appid(), which looks for the GUID path for the given appname.

        Args:
            wait_after (float, optional): Wait time in s after app launch. Defaults to 1.
        """
        if self.appname:
            wr2_app_launcher = get_appid()
            wr2_process = subprocess.Popen(f"explorer shell:appsFolder\\{wr2_app_launcher}")
            time.sleep(wait_after)
            if not wr2_process.returncode:
                self.app_launched = True

    def _callback_is_in_windowname(self, hwnd: int, substring: str):
        """
        Callback function to check if a substring of the window title is in the window
        with handle hwnd. This should only be used inside the find window method.

        This variant of the callback looks for substring and may not be accurate if there are
        multiple windows open with the same substring in window title.

        The method is however needed - I think - to take control over the Winriver 2 main window
        because the window title of this one changes with various data acquisition states.

        Args:
            hwnd (int): Window handle of window to check for substring in title
            substring (str): The substring in window title to look for
        """
        self._substr = substring
        window_text = str(win32gui.GetWindowText(hwnd))
        if substring in window_text:
            logger.debug(f"Found window with name {window_text} with HWND {hwnd}")
            self.hwnd = hwnd
            self.window_text = window_text
    
    def _callback_is_exactly_in_windowname(self, hwnd: int, exact_title: str):
        """
        Callback function to check if a given window with handle hwnd has the exact title. This 
        should only be used inside the find window method.

        Behaviour is untested for situation with several windows with the exact title but 
        different window handles.

        Args:
            hwnd (int): Window handle of window to check for exact_title in title
            exact_title (str): The exact title in window title to look for
        """
        self._substr = exact_title
        window_text = str(win32gui.GetWindowText(hwnd))
        if exact_title == window_text:
            logger.debug(f"Found window with name {window_text} with HWND {hwnd}")
            self.hwnd = hwnd
            self.window_text = window_text

    def find_window(self, title: str, exact_match: bool = False):
        """
        Iterates through all open windows and checks if a certain window has the desired title.

        Args:
            title (str): The desired title of the window to find.
            exact_match (bool, optional): Switches the callback function. Defaults to False.
        """
        if exact_match:
            win32gui.EnumWindows(self._callback_is_exactly_in_windowname, title)
        else:
            win32gui.EnumWindows(self._callback_is_in_windowname, title)
        
    def window_exists(self):
        """Intended as helper to check if the window handled by self exists"""
        if self.hwnd > 0:
            return True
        else:
            False
    
    def bring2front(self):
        """
        Simple wrapper around win32gui.SetForegroundWindow.

        Raises:
            ValueError: If hwnd is not a positive integer
        """
        if self.hwnd > 0:
            win32gui.SetForegroundWindow(self.hwnd)
        else:
            logger.error(f"No Valid HWND for window title with substr {self._substr}")
            raise ValueError("Cannot bring window to without valid HWND")
        
    def query_process_memory(
        self, 
        reqAc: int = win32con.PROCESS_QUERY_LIMITED_INFORMATION, 
        mem_info_key: str = "WorkingSetSize"
        ) -> int:
        """
        Query process memory information. This is a convenience wrapper around several win32 API
        calls:
        1. GetWindowThreadProcessId(hwnd), which returns the ids for (thread, process), only process is used
        2. OpenProcess to get a pyHANDLE for the desired process, use reqAc to set the appropriate access rights
        3. GetProcessMemoryInfo to obtain a dict with several memory information. Since this 
        tool is most interested in WorkingSetSize this is the default key. Others may be used.

        Args:
            reqAc (int, optional): Request Access rights for process. Defaults to win32con.PROCESS_QUERY_LIMITED_INFORMATION.
            mem_info_key (str, optional): Which memory info to use as dict key. Defaults to "WorkingSetSize".

        Returns:
            int: The memory used in bytes
        """

        logger.debug(f"Querying memory info for window {self.window_text} with id {self.hwnd}")
        mi = win32process.GetProcessMemoryInfo(
            win32api.OpenProcess(reqAc, 1, win32process.GetWindowThreadProcessId(self.hwnd)[1])
        )
        return mi[mem_info_key]

    def show_window(self, prop: int):
        """
        Wrapper around win32gui.ShowWindow() function to set the maximized / minimized property.

        Args:
            prop (int): An int presumably in range 0 ... 3, i.e. win32com.SW states.
        """
        win32gui.ShowWindow(self.hwnd, prop)

    def close_window(self):
        """Helper function to close a window normally."""
        logger.info(f"Closing window for app {self.window_text}")
        win32gui.PostMessage(self.hwnd, win32con.WM_CLOSE, 0, 0)

def _str2path(s: str) -> pathlib.Path:
    """
    Convenience function to typecast str paths to Path Objects

    Args:
        s (str): Valid path as str

    Returns:
        pathlib.Path: Path Object constructed from s
    """
    logger.debug("Parsed class str filepath to class pathlib.Path")
    return pathlib.Path(s)

def ensure_file_existence(filepath: str | pathlib.Path, create_file: bool = True) -> int:
    """
    Helper function to check whether a file exists and create it if not. This works only for
    TextIO mode and will create an empty file.

    Args:
        filepath (str | pathlib.Path): Which file to check.
        create_file (bool, optional): If True create the file. Defaults to True.

    Returns:
        int: 0 if successful, otherwise 1.
    """
    if isinstance(filepath, str):
        filepath = _str2path(filepath)
    if not os.path.isfile(filepath):
        logger.debug(f"File {filepath} does not exist")
        if create_file:
            with open(filepath, "w", encoding="cp1252") as ft:
                logger.info(f"Created empty file {filepath.name}")
            return 0
        else:
            logger.warning(f"Did not create file {filepath}")
            logger.warning("If this is undesired pass create_file=True")
            return 1
    else:
        return 0
        
def ensure_dir_existence(dirpath: str | pathlib.Path, mkdir: bool = True) -> int:
    """
    Helper function to check if a directory exists and try to make it if not. Note that 
    creation only works if the parents exists.

    Args:
        dirpath (str | pathlib.Path): The directory to check or create.
        mkdir (bool, optional): If true try to make the dir. Defaults to True.

    Returns:
        int:  0 if successful, otherwise 1.
    """
    if isinstance(dirpath, str):
        dirpath = _str2path(dirpath)
    if not dirpath.is_dir():
        logger.debug(f"Directory {dirpath} does not exist")
        if mkdir:
            dirpath.mkdir()
            logger.info(f"Created directory {dirpath}")
            return 0
        else:
            logger.warning(f"Did not create dir {dirpath}")
            logger.warning("If this is undesired pass mkdir=True")
            return 1
    else:
        return 0
    
def move_processed_PD0_files(
        tracking_file: str | pathlib.Path,
        trgt_path: str | pathlib.Path
        ) -> int:
    ft = open(tracking_file, "r+", encoding="cp1252")
    # move old pd0 files in project dir
    for f in ft.readlines():
        f = pathlib.Path(f.strip('\n'))
        try:
            shutil.move(f, trgt_path / f.name)
            logging.info(f"Moved file to processed: {f}")
        except FileNotFoundError:
            logging.error(f"Could not find file: {f}")
    ft.truncate()
    ft.close()
    return 0

if PYDCP2:
    def _fill_raw_data_gaps(datastruct: pydcp2.io.DataStruct, keys: list[str], fill_value: float = -999.):
        for k in keys:
            if k not in datastruct:
                logging.warning(f"Key {k} not present in datastruct, using fill value")
                datastruct.add_variable(k, dims=1, dtype="f4")
                datastruct.store_value(k, slice(None), fill_value)
            else:
                pydcp2.processing.interpolate_bad_1d(datastruct, k, "int timestamp", bad_val=fill_value)
        return datastruct
    
    def clean_processing_dir(
            tracking_file: pathlib.Path,
            pydcp_basepath: pathlib.Path,
            pd0_src_path: pathlib.Path,
            nc_global_attr_file: pathlib.Path,
            nc_global_attr_src: pathlib.Path,
            processed_path: str = "processed"
        ) -> None:
        """
        Helper function to clean the pydcp2 processing directory tree before doing the processing.
        Calling this function can cause data loss, so think before calling it!

        Args:
            pydcp_basepath (pathlib.Path): The basepath of the processing dir.
            nc_global_attr_file (pathlib.Path): Where to put the global netCDF Attribute template
            nc_global_attr_src (pathlib.Path): Sourcepath of netCDF Attribute Template
        """
        ppath = pd0_src_path / processed_path
        ensure_dir_existence(pydcp_basepath)
        ensure_dir_existence(ppath)
        ensure_file_existence(nc_global_attr_file)
        ensure_file_existence(nc_global_attr_src)
        move_processed_PD0_files(tracking_file, ppath)
        pydcp2.io.tree_maker(pydcp_basepath, clean=True)
        shutil.copy(nc_global_attr_src, nc_global_attr_file)

    def PD0_to_geojson(
            tracking_file: pathlib.Path,
            pydcp_basepath: pathlib.Path,
            pd0_src_path: pathlib.Path,
            geojson_trgt_path: pathlib.Path,
            ) -> None:
        
        ensure_file_existence(tracking_file)
        ensure_dir_existence(pydcp_basepath)
        ensure_dir_existence(geojson_trgt_path)

        ft = open(tracking_file, "r+", encoding="cp1252")
        t0 = time.perf_counter()
        all_files = list(pd0_src_path.glob("*.PD0"))
        n_all_files = len(all_files)
        processed_files = [pathlib.Path(_.strip('\n')) for _ in ft.readlines()]
        pf_set = set(processed_files)
        af_set = set(all_files)
        new_pd0_files = af_set.difference(pf_set)
        logger.debug(f"Nall_files={n_all_files}, Nprocessed_files={len(processed_files)}, Nnew_PD0_files={len(new_pd0_files)}")
        n_new_files = 0

        for pd0_file in new_pd0_files:
            file_age =  datetime.datetime.now().timestamp() - os.path.getmtime(pd0_file)
            if (
                file_age > 10 and # only completely written files
                os.path.getsize(pd0_file) > 10240 # this should prevent processing the initial mini pd0 files
                ):
                # 1. copy new pd0 files into processing
                shutil.copy2(pd0_file, pydcp_basepath / "ensembles")
            
                # 2. pydcp2 processing
                try:
                    sname = pathlib.Path(pd0_file).name
                    logger.info(f"Processing {sname}")
                    rdi = trdi.TRDI_Decoder(pd0_file)
                    rdi.decode()
                    ds = rdi.get_datastruct()
                    # fill gaps in raw data
                    ds = _fill_raw_data_gaps(ds, "gga latitude,gga longitude,hdt heading,gga utc,vtg sog".split(','))
                    
                    # elevate precedence of certain variables
                    pydcp2.processing.make_variable_main(ds, "gga longitude")
                    pydcp2.processing.make_variable_main(ds, "gga latitude")
                    pydcp2.processing.make_variable_main(ds, "hdt heading")
                    pydcp2.processing.make_variable_main(ds, "int pitch")
                    pydcp2.processing.make_variable_main(ds, "int roll")
                    pydcp2.processing.make_variable_main(ds, "int timestamp")

                    # transform ship coordinate system into earth coordinate system using bottom track as v ref
                    pydcp2.processing.rdi_ship2earth(ds)
                    pydcp2.processing.mark_excessive_earth_velocities_bad(ds, threshold=3)
                    pydcp2.processing.compute_bindepths(ds, xdcr_depth=1.7)
                    pydcp2.processing.bottom_from_bottom_track(ds, 1.7, backstep=1)
                    # compute the average across 10 pings
                    avg = pydcp2.processing.compute_ensemble_averages_n(ds, n=10, min_good_sample_fraction=0.5)
                    # gj_path = pydcp2.geo.geoJSON_from_datastruct(avg, pydcp_basepath / "KML" / f"{sname.strip('.PD0')}.geojson", skip_bad_data=True)
                    gj_path = pydcp2.geo.geoJSON_from_datastruct(
                        avg,
                        pydcp2.geo.geojson_pfunc_generic_annotation(avg, "RDI WH, 600 kHz, SN15843", name="Instrument"),
                        pydcp2.geo.geojson_pfunc_magnitude(avg, include_qf0=True, format_as_type=float),
                        pydcp2.geo.geojson_pfunc_direction(avg, include_qf0=True, format_as_type=float),
                        pydcp2.geo.geojson_pfunc_qf(avg, 0, 3),
                        pydcp2.geo.geojson_pfunc_generic_tsdata(avg, key="bottom depth", name="sea_floor_depth_below_sea_surface", units="m"),
                        pydcp2.geo.geojson_pfunc_generic_tsdata(avg, key="int temperature", name="sea_water_temperature", units="degree_C"),
                        filepath=pydcp_basepath / "KML" / f"{sname.strip('.PD0')}.geojson"
                        )         
                # here we could get a clue of what went wrong during processing. this should stay i guess
                except Exception as e:
                    logger.error(f"Caught exception {e} in _process_PD0_file")

                # 4. copy geojson to ludmila transfer dir
                if gj_path:
                    shutil.copy2(gj_path, geojson_trgt_path)
                    logger.info(f"Created geojson file: {gj_path.name}")
                else:
                    logger.warning("Variable gj_path was false, there is no geojson output wirtten. This may be normal if all bins are bad, though.")

                ft.write(f"{pd0_file}\n")
                n_new_files += 1

        t1 = time.perf_counter()

        logger.debug(f"Processing took {(t1-t0)*1000:.3f} ms, Files: {n_all_files}, New files: {n_new_files}")
        logger.info(f"File with processed paths written to {tracking_file}")
        
        if not ft.closed:
            ft.close()

def get_appid(appname: str = "WinRiver.exe", remove_app_id_file: bool = True) -> str:
    """
    Get the appId of a given appname. This function uses a windows powershell subprocess to 
    obtain a list of all apps listed in Get-StartApps. Using the GUID paths found like this 
    seems to be the only reliable way to launch Winriver 2 without extensive cd-ing through
    directories.

    Args:
        appanme (str, optional): The app to look for. Defaults to Winriver.exe.
        remove_app_id_file (bool, optional): Remove the temporary file with listed app ids. 
        Defaults to True.

    Returns:
        appId_lp (str): The launch path for the app with appname
    """
    
    temp_out_fn = os.path.join(os.path.expanduser("~"), "Documents", "temp_app_ids.txt")
    subprocess.run(f"""powershell -command "Get-StartApps | Out-File -encoding ASCII -width 800 -FilePath '{temp_out_fn}'; exit" """)
    appId_lp = ""
    with open(temp_out_fn, "r") as fn:
        for line in fn.readlines():
            if appname in line:
                appId_lp = line[line.find("{") :].strip("\n")
    if not appId_lp:
        logger.error(f"Could not find {appname}")
        raise RuntimeError(f"Could not find {appname}, is it installed?")
    else:
       logger.info(f"Found {appname} Launcher with AppID: {appId_lp}")
    if remove_app_id_file:
        try:
            os.remove(temp_out_fn)
        except Exception as e:
           logger.debug(f"Could not remove tempory file: {temp_out_fn}")
           logger.debug(f"Reason: {e}")
    return appId_lp

def send_key_to_window(twm: SimpleWindowManager, shell_obj, key: str, wait_after: float = 1):
    """
    Wrapper around WScript.shell SendKeys function. Adds some level of control where the virtual
    key stroke goes by bringen the desired window to foreground before issuing the key stroke.

    Args:
        twm (SimpleWindowManager): Manages the window the key stroke is supposed to go to.
        shell_obj (_type_): The instance of the WScript.shell to use.
        key (str): Key or str of keys to send.
        wait_after (float, optional): Defines a waiting time after sending the key stroke. This
        may be useful if the reaction to the key takes some time. Given in s. Defaults to 1.
    """
    twm.bring2front()
    shell_obj.SendKeys(key)
    time.sleep(wait_after)
    logger.debug(f"Sent key '{key}' to window {twm.window_text}, waited {wait_after} s")

def set_adcp_clock(shell_obj):
    """
    A sequence of send keys to set the ADCP clock, will only work if the set ADCP clock window
    was already opened.

    TODO: Add exception handling?!

    Args:
        shell_obj (WScript.shell): The shell scripting object with its SendKeys method.
    """
    clock_wm = SimpleWindowManager()
    clock_wm.find_window(wr2gui.WND_SETCLOCK)
    time.sleep(1)
    send_key_to_window(clock_wm, shell_obj, VK_TAB)
    send_key_to_window(clock_wm, shell_obj, VK_UP)
    send_key_to_window(clock_wm, shell_obj, VK_TAB)
    send_key_to_window(clock_wm, shell_obj, VK_SPACE)
    send_key_to_window(clock_wm, shell_obj, VK_TAB)
    send_key_to_window(clock_wm, shell_obj, VK_ENTER, wait_after=2)
    clock_confirm_wm = SimpleWindowManager()
    clock_confirm_wm.find_window(wr2gui.WND_CLOCK, exact_match=True)
    send_key_to_window(clock_confirm_wm, shell_obj, VK_ENTER)
    del(clock_confirm_wm)
    send_key_to_window(clock_wm, shell_obj, VK_TAB)
    send_key_to_window(clock_wm, shell_obj, VK_ENTER, wait_after=30)
    logger.info("Wait 30 s for Instrument programming and stable pinging")

def update_mmt(mmt_path: str, target_dir: str):
    """
    Takes the default .mmt and updates the timestamp of the first file. Currently hardcoded for
    the config on Ludwig Prandtl.

    Most likely this function can be removed, as it is not necessary anymore and adds a space
    for potential erros.

    Args:
        mmt_path (str): _description_
        target_dir (str): _description_
    """    
    logger.info("Updating .mmt file for next data collection cycle")
    current_datetime = datetime.datetime.now().replace(tzinfo=datetime.timezone.utc)
    cd_str = current_datetime.strftime("%y-%m-%d_%H%M%S")
    
    mmt_config_name = mmt_path.split(os.path.sep)[-1]

    new_mmt_content = ""
    with open(mmt_path, "r", encoding="cp1252") as mmt:
        mmt_content = mmt.read()
    
    fi0 = 0
    fi1 = 0
    # finding the first file entry
    fi0 = mmt_content.find("<File PathName=") + 16 # characters from find to start of path...
    fi1 = fi0 + mmt_content[fi0:].find("Type=") - 2 # same magic number as above
    new_mmt_content += mmt_content[:fi0] # writes the reusable content to the new mmt file content
    # set new timestamp for PD0 file
    absolute_fp = f"{mmt_content[fi0:fi1-19]}{cd_str}.PD0"
    new_mmt_content += absolute_fp
    # add reuseable content
    fi0 = fi1
    fi1 = fi0 + mmt_content[fi0:].find(">") + 1
    new_mmt_content += mmt_content[fi0:fi1]
    # modify transect id
    fi0 = fi1
    fi1 = fi0 + mmt_content[fi0:].find("<")
    transect_id = f"{mmt_content[fi0:fi1-19]}{cd_str}.PD0"
    new_mmt_content += transect_id
    logger.info("Updated .mmt timestamps for file format PD0")
    # add reuseable content up to next
    while mmt_content[fi0:].find("<File PathName=") != -1:
        fi0 = fi1
        fi1 = fi0 + mmt_content[fi0:].find("<File PathName=") + 16
        new_mmt_content += mmt_content[fi0:fi1]
        # set new timestamp for GPS or EH file
        fi0 = fi1
        fi1 = fi0 + mmt_content[fi0:].find("Type=") - 2
        absolute_fp = mmt_content[fi0:fi1]    
        stream_type = _get_stream_id(absolute_fp)
        absolute_fp = f"{absolute_fp[:(-19 - 1 - len(stream_type))]}{cd_str}_{stream_type}.TXT"
        new_mmt_content += absolute_fp
        # add reuseable content
        fi0 = fi1
        fi1 = fi0 + mmt_content[fi0:].find(">") + 1
        new_mmt_content += mmt_content[fi0:fi1]
        # modify transect id of stream_type file
        fi0 = fi1
        fi1 = fi0 + mmt_content[fi0:].find("<")
        transect_id = f"{mmt_content[fi0:fi1-(19 + 1 + len(stream_type))]}{cd_str}_{stream_type}.TXT"
        new_mmt_content += transect_id
        logger.info(f"Updated .mmt timestamps for file format {stream_type}.TXT")
    new_mmt_content += mmt_content[fi1:]
    
    # write updated .mmt to project dir
    with open(os.path.join(target_dir, mmt_config_name), "w", encoding="cp1252") as new_mmt:
        new_mmt.write(new_mmt_content)

def _get_stream_id(filepath: str) -> str:
    """
    Helper to get the data stream ID from WR2 aux file names. Probably not needed anymore.
    
    Will be removed if function update_mmt is removed.
    """
    if "EH" == filepath.split(os.path.sep)[-1].split('.')[0].split("_")[-1]:
        return "EH"
    elif "GPS" == filepath.split(os.path.sep)[-1].split('.')[0].split("_")[-1]:
        return "GPS"
    elif "SND" == filepath.split(os.path.sep)[-1].split('.')[0].split("_")[-1]:
        return "SND"
    else:
        logger.error("Unexpected file data type identifier found in .mmt file.")
        raise RuntimeError("Unexpected data type identiefier for .TXT file in -mmt found")

def copy_config_file(source_file: str, target_dir: str):
    """
    Wrapper around shutil.copy2 to copy wr2 config files to restore a known state

    Args:
        source_file (str): File to copy
        target_dir (str): Dir to copy source file to
    """    
    shutil.copy2(source_file, target_dir)
    logger.info(f"Copied config file {source_file} to {target_dir}")

def copy_data_files(src: str, trgt: str):
    """
    Copies all data files collected today (datetime.date.today()) to another directory
    with date in directory name. This function is called during shutdown of the ADCP PC
    and creates a backup copy of the data files from the default project directory. Files
    in the project directory are not touched.

    Function does not create a secure backup because the copies will be stored on the same 
    hard drive...

    Args:
        src (str): Source directory
        trgt (str): Target directory
    """
    if not os.path.exists(trgt):
        logger.warning(f"Target dir does not exist: {trgt}")
        logger.info(f"Try to create target directory")
        try:
            os.mkdir(trgt)
        except Exception as e:
            logger.error(f"Could not create target directory: {e}")

    today = datetime.date.today()
    current_date_dir_fmt = today.strftime("%Y%m%d")
    current_date_file_fmt = today.strftime("%y-%m-%d")
    files = glob.glob(os.path.join(src, f"*{current_date_file_fmt}*"))
    trgt = os.path.join(trgt, f"data_cp_{current_date_dir_fmt}")
    if not os.path.exists(trgt):
        os.mkdir(trgt)
    for f in files:
        shutil.copy2(f, trgt)
        logger.info(f"Copied file {f} to {trgt}")

def _compute_file_hashes(files: list[str]) -> dict:
    """
    Computes the SHA1 hash of the files given as input. Files are opened in binary mode. Function
    is used to identify files with the same content but different names. This is necessary because
    data files collected on the ADCP PC are copied into a transfer directory and if successfully
    transferred are renamed.

    Args:
        files (list[str]): A list of absolute filepaths as strings.
        ignore_transferred (bool, optional): If true do not transfer already transferred file. Defaults to False.
        transferred_id (str, optional): A substring present in files that have already been
        transferred. Defaults to S_ .

    Returns:
        list: List of SHA1 hashes, hexadecimal integers
    """

    t0 = perf_counter()
    hashes = {}
    for f in files:
        with open(f, "rb") as f_obj:
            f_hash = hashlib.sha1(f_obj.read()).hexdigest()
            hashes[f_hash] = f
            logger.debug(f"{f_hash[:10]}: {pathlib.Path(f).name}")
    t1 = perf_counter()
    logger.debug(f"Computed hashes for {len(hashes)} files in {t1-t0:.1f} s")
    return hashes

def copy_data_files4ludmila(
    src: str, 
    trgt: str, 
    mtime_delay: int | float = 10,
    force: bool = False
    ):
    """
    Copies files written by ADCP DAS to trgt directory. Files copied there will be fetched
    by another Software von board RV Ludwig Prandtl called LUDMILA via FTP. LUDMILA takes a copy
    of all files and if copy was succesfull it renames the files in trgt dir by prepending a 'S\_'
    to each file. Therefore simple filename checking is not sufficient to check if a file already
    exists.

    Files that are still in use by Winriver2 could potentially be transferred before Winriver2 
    finishes writing them. Therefore the timedelta between a datetime.datetime.now() and
    os.path.getmtime() is computed. If Winriver is still writing to a file this value will be
    smaller or equal to the ping / ensemble time set in Winriver. Set mtime_delay accordingly.

    Args:
        src (str): Source directory for files to copy
        trgt (str): Target file directory to copy to
        mtime_delay (int | float, optional): Defines the number of seconds a files getmtime must
        be older than now to be considered completely written.
    """
    if not os.path.exists(trgt):
        logger.warning(f"Target dir does not exist: {trgt}")
        logger.info(f"Try to create target directory")
        try:
            os.mkdir(trgt)
        except Exception as e:
            logger.error(f"Could not create target directory: {e}")

    today = datetime.date.today()
    current_date_file_fmt = today.strftime("%y-%m-%d")
    if force:
        canditates_files = glob.glob(os.path.join(src, f"*.PD0"))
        canditates_files.extend(glob.glob(os.path.join(src, f"*.TXT")))
    else:
        canditates_files = glob.glob(os.path.join(src, f"*{current_date_file_fmt}*"))
    logger.info(f"Found {len(canditates_files)} candidate files to copy")
    src_files = []
    for cf in canditates_files:
        td = datetime.datetime.now().timestamp() - os.path.getmtime(cf)
        if td > mtime_delay and td < 180 * 86400:
            src_files.append(cf)
    logger.debug("Computing source file hashes")
    src_file_hashes = _compute_file_hashes(src_files)
    if force:
        trgt_files = glob.glob(os.path.join(trgt, f"*"))
    else:
        trgt_files = glob.glob(os.path.join(trgt, f"*{current_date_file_fmt}*"))
    logger.debug("Computing target file hashes")
    trgt_file_hashes = _compute_file_hashes(trgt_files)#, ignore_transferred=ignore_transferred, transferred_id=transferred_id)
    for sf_hash, sf in zip(src_file_hashes, src_files):
        if sf_hash in trgt_file_hashes:
            logger.info(f"File {sf} already present in target dir, skipping")
            existing_file = trgt_file_hashes[sf_hash]
            logger.info(f"Existing filename is {existing_file}")
            continue
        shutil.copy2(sf, trgt)
        logger.info(f"Copied file {sf} to {trgt}")

def _accumulate_sp_message(sp: serial.Serial) -> str:
    """
    Collects bytes from serial port buffer until empty byte. Returns cp1252 decoded version of
    the accumulated message

    Args:
        sp (serial.Serial): serial port object

    Returns:
        str: Received string
    """
    msg = b""
    c = sp.read(1)
    while c != b"":
        msg += c
        c = sp.read(1)
    return msg.decode(encoding="cp1252")

def shutdown_RDI_ADCP(serialport: str, baudrate: int):
    """
    Convenienve wrapper around serial port communcation with RDI ADCP to power it down
    using the workhorse command CZ.

    Args:
        serialport (str): The serial port name the ADCP is attached to. On Windows something
        like COM<nn>
        baudrate (int): Baudrate of the before specified serial port
    """
    with serial.Serial(port=serialport, baudrate=baudrate, timeout=0.1) as sp:
        logger.debug(f"Opened serial port {serialport} with baudrate {baudrate}")
        time.sleep(1)
        sp.reset_input_buffer()
        sp.send_break(duration=0.5)
        logger.debug("Sent break")
        time.sleep(2)
        msg = _accumulate_sp_message(sp)
        logger.debug(f">>>  {msg}")
        time.sleep(3)
        sp.reset_input_buffer()
        logger.debug("Reset input buffer")
        sp.write("cz\r".encode())
        logger.debug("send power down message cz")
        msg = _accumulate_sp_message(sp)
        logger.debug(f">>>  {msg}")

def show_cmd():
    """
    Helper function to bring console window (cmd on windows) to front.
    """
    logger.debug("Trying to bring info window to front")
    try:
        cwm = SimpleWindowManager()
        cwm.find_window("startwr2")
        cwm.show_window(win32con.SW_SHOWNORMAL)
    except Exception as e:
        logger.debug(f"Exception >>{e}<< catched. This may be expected.")

def kill_running_instances(window_title: str) -> None:
    """
    Look for windows with window_title and successivly kill all instances running.
    This is needed to ensure that only one instance of a window is running and to 
    ensure that all virtual keystrokes arrive at the same instance.

    TODO: The whole window handling could be done more elegant if SimpleWindowManager
    would be aware of ALL running instances and would refer to the correct one by hWnd.

    Args:
        window_title (str): A substring to be found in the window title.
    """
    while True:
        wm = SimpleWindowManager()
        wm.find_window(window_title)
        if not wm.window_exists():
            break
        logger.debug(f"Kill window: {wm.window_text}, hwnd: {wm.hwnd}")
        win32gui.PostMessage(wm.hwnd, win32con.WM_DESTROY, 0, 0)
        time.sleep(1)

def start_up(
    project_base_path: str,
    mmt_src_path: str,
    wsp_src_path: str,
    ):
    """
    Collection of commands and functions needed to fully launch a WR2 instance and start 
    collection data. 

    #TODO potential for refactoring
    """
    print("Starting WinRiver 2 automatically\n")
    print("#############################################################")
    print("#                                                           #")
    print("# Please wait until WinRiver 2 shows Pinging AND Recording! #")
    print("#                                                           #")
    print("#############################################################\n")
    for s in range(10,0,-1):
        time.sleep(1)
        print(f"{s} ... ", end="")
        sys.stdout.flush()
    print('\n')
    logger.info("Started script, waiting for bootup")
    # this should be removed after removing the .cmd scripts
    try:
        cwm = SimpleWindowManager()
        cwm.find_window("pyenv\Scripts")
    except Exception as e:
        logger.debug(f"Exception >>{e}<< catched. This may be expected.")
    copy_config_file(mmt_src_path, project_base_path)
    copy_config_file(wsp_src_path, project_base_path)
    kill_running_instances("WinRiver")
    wm = SimpleWindowManager(appname="WinRiver.exe")
    wm.launch_app()
    time.sleep(5)
    wm.find_window(wr2gui.WND_MAIN)
    try:
        cwm.bring2front()
    except Exception as e:
        logger.debug(f"Exception >>{e}<< catched. This may be expected.")
    logger.info("Starting WScript.Shell")
    wsh = win32com.client.Dispatch("Wscript.Shell")
    logger.debug("WScript.Shell started")
    logger.debug("Waiting for Windows to settle")
    try:
        cwm.bring2front()
    except Exception as e:
        logger.debug(f"Exception >>{e}<< catched. This may be expected.")
    time.sleep(4)

    mmt_path = os.path.join(
        project_base_path, mmt_src_path.split(os.path.sep)[-1]
        )
    wsp_path = os.path.join(
        project_base_path, wsp_src_path.split(os.path.sep)[-1]
        )
    
    # load .wsp
    send_key_to_window(wm, wsh, VK_ALT, wait_after=0.5)
    send_key_to_window(wm, wsh, VK_ENTER, wait_after=0.5)
    send_key_to_window(wm, wsh, f"{8*VK_DOWN}", wait_after=0.05) # 8 times down for WR216
    send_key_to_window(wm, wsh, VK_ENTER)
    wspwm = SimpleWindowManager()
    wspwm.find_window(wr2gui.WND_OPENWSP)
    send_key_to_window(wspwm, wsh, wsp_path, wait_after=0.5)
    send_key_to_window(wspwm, wsh, VK_ENTER)
    del(wspwm)

    # load .mmt
    send_key_to_window(wm, wsh, f"{VK_CTRL}o")
    fowm = SimpleWindowManager()
    fowm.find_window(wr2gui.WND_OPENMMT)
    send_key_to_window(fowm, wsh, mmt_path, wait_after=0.5)
    send_key_to_window(fowm, wsh, VK_ENTER)
    del(fowm)
    
    # start pinging
    logger.info("Try to start pinging using VK_F4")
    send_key_to_window(wm, wsh, VK_F4, wait_after=10)

    # set adcp clock to GPS ZDA
    logger.info("Try to set ADCP clock")
    set_adcp_clock(wsh)
    try:
        cwm.bring2front()
    except Exception as e:
        logger.debug(f"Exception >>{e}<< catched. This may be expected.")

    # start transect
    logger.info("Try to start transect")
    send_key_to_window(wm, wsh, VK_F5)
        # in WR226 this can throw a window asking about compass calib
    stwm = SimpleWindowManager()
    stwm.find_window(wr2gui.WND_STARTTRANSECT)
    send_key_to_window(stwm, wsh, VK_ENTER, wait_after=0.5)
    # immediate end to overcome missing EH.TXT bug
    #TODO: CHECK IF END TRANSEKT WINDOW!
    send_key_to_window(wm, wsh, VK_F5)
    send_key_to_window(stwm, wsh, VK_ENTER, wait_after=1)
    # final start
    send_key_to_window(wm, wsh, VK_F5)
    send_key_to_window(stwm, wsh, VK_ENTER, wait_after=1)
    wm.show_window(win32con.SW_MAXIMIZE)
    del(stwm)
    try:
        cwm.bring2front()
    except Exception as e:
        logger.debug(f"Exception >>{e}<< catched. This may be expected.")
    logger.info("You may now start to work, this console will close in 1 min.")
    print()
    for s in range(60,0,-1):
        time.sleep(1)
        print(f"\rWindow closes automatically in {s:2} s", end="")
        sys.stdout.flush()
    print()

def quit_wr2(
        project_base_path: str,
        file_trgt_path: str,
        ludmila_trgt_path: str
        ):
    """the following block would be needed if the script could be called before the 
    power button signal is issued. Looks like windows kills all running processes with
    Terminate and also kills the gui toolkit first. 

    I have not yet an idea how to shut winriver cleanly except for running something on
    LUDMILA PC and communication VIA network... 

    Then again the only consequence of killing wr2 is that the MMM is not written clean. Tha
    data files are ok.
    """

    logger.info("Startet shutdown script")
    
    try:
        logger.info("Trying to close WR2 normally")
        logger.info("Starting WScript.Shell")
        wsh = win32com.client.Dispatch("Wscript.Shell")
        logger.debug("WScript.Shell started")
        wm = SimpleWindowManager()
        wm.find_window(wr2gui.WND_MAIN)
        time.sleep(1)
        send_key_to_window(wm, wsh, VK_F5)
        etwm = SimpleWindowManager()
        etwm.find_window(wr2gui.WND_ENDTRANSECT)
        send_key_to_window(etwm, wsh, VK_ESC)
        del(etwm)
        send_key_to_window(wm, wsh, VK_F4, wait_after=2)
        wm.close_window()
    except Exception as e:
        logger.debug(f"During shutdown exception >>{e}<< occured")
        logger.info("WR2 was not closed normally, this may however be expected when shutdown with power button")
    finally:
        del(wsh, wm)
    time.sleep(3)
    copy_data_files(project_base_path, file_trgt_path)
    copy_data_files4ludmila(project_base_path, ludmila_trgt_path)
    logger.info("File moving and ADCP power down completed, going to power off machine in 5s")

def copy_files2transfer_dir(src: str, dst: str, force: bool = False) -> None:
    logger.info(f"Copy script run at time: {datetime.datetime.now().strftime('%H:%M:%S')}")
    copy_data_files4ludmila(src, dst, force = force)

def change_file_handler(logger: logging.Logger, log_path: pathlib.Path, fmt: str, name: str = ""):
    """
    Helper function to remove existing file handlers from logger and set a new one for use
    in different argparse settings.

    Args:
        logger (logging.Logger): Instance of logger class#
        log_path (pathlib.Path): Where the log files should be saved.
        fmt (str): The format str used by the logger
        name (str, optional): A str that appears in the log file name. Defaults to "".
    """
    if name and name[0] != '_':
        name = f"_{name}"
    _log_file = log_path / f"auto_adcp_{datetime.datetime.now().strftime('%Y%m%dT%H%M%S')}{name}.log"
    _file_handler = logging.FileHandler(_log_file)
    _file_handler.setLevel(logging.DEBUG)
    _file_handler.setFormatter(fmt)
    # look for exisitng filehandlers to prevent duplicates
    for h in logger.handlers:
        if isinstance(h, logging.FileHandler):
            logger.removeHandler(h)
    logger.addHandler(_file_handler)
    logger.info(f"Logging to {_log_file}")

def is_app_memory_less(
        appname: str = "WinRiver II",
        max_memory: int = 1E8,
        memkey: str = "WorkingSetSize"
        ) -> bool:
    logger.debug(f"Checking memory usage for app '{appname}' for memory set: {memkey}")
    wm = SimpleWindowManager()
    wm.find_window(appname)
    m = wm.query_process_memory(mem_info_key=memkey)
    logger.debug(f"Memory usage is {int(m / 1E6)} MB for app {appname}")
    if m < max_memory:
        logger.info("Memory usage is within requested limit")
        return True
    else:
        logger.info("Memory usage is larger than requested limit")
        return False

def restart_data_acquisition(appname: str = "WinRiver II") -> None:
    try:
        logger.info("Restarting data acquisition")
        wm = SimpleWindowManager()
        wm.find_window(appname)
        wsh = win32com.client.Dispatch("Wscript.Shell")
        # end transect
        send_key_to_window(wm, wsh, VK_F5, wait_after=2)
        etwm = SimpleWindowManager()
        etwm.find_window(wr2gui.WND_ENDTRANSECT)
        send_key_to_window(etwm, wsh, VK_ENTER)
        # stop pinging
        send_key_to_window(wm, wsh, VK_F4, wait_after=15)
        # start pinging
        send_key_to_window(wm, wsh, VK_F4, wait_after=45)
        # start transect
        send_key_to_window(wm, wsh, VK_F5, wait_after=3)
        stwm = SimpleWindowManager()
        stwm.find_window(wr2gui.WND_STARTTRANSECT)
        send_key_to_window(stwm, wsh, VK_ENTER)
        logger.info("Restartet data acquisition in WR2")
    except Exception as e:
        logger.error(e)
        raise e
    finally:
        logger.debug("Finished in finally block")