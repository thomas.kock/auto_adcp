"""
auto_adcp command line interface.
Author: Thomas Kock
E-Mail: thomas.kock@hereon.de
Copyright holder: Helmholtz-Zentrum Hereon GmbH
Date: 2024-01-04
"""

__version_major__ = 0
__version_minor__ = 4

import argparse, configparser, logging
import pathlib, os.path, sys, time
from  auto_adcp import lib_auto_adcp as libaa

if "pythonw.exe" in sys.executable:
    HAS_CONSOLE = False
else:
    HAS_CONSOLE = True

logger = logging.getLogger()

# The ini file is located in the same repo than the source code
_cfg_path = pathlib.Path(os.path.dirname(__file__)).parent / "resource/auto_adcp_config.ini"
_cfg = configparser.ConfigParser()
_cfg.read(_cfg_path)
_log_path = pathlib.Path(_cfg["Logging"]["logPath"])

# set up argparse for handling CLI arguments
_ap = argparse.ArgumentParser(
    usage="python -m auto_adcp [options]",
    description="auto_adcp is a tool to launch Winriver 2 after windows logon and start data acquistion. It can also handle file moving at shutdown."
)
_ap.add_argument("--show_cmd", help="Use this option to bring the start up script to front, i.e. make it visbile", action="store_true")
_ap.add_argument("--debug", help="For testing of things", action="store_true")
_ap.add_argument("--startwr2", help="Call Winriver 2 logon script", action="store_true")
_ap.add_argument("--clean_pdir", help="Clean up the processing dir if available", action="store_true")
_ap.add_argument("--shutdown", help="Clean up after at logoff or when Winriver2 should be closed", action="store_true")
_ap.add_argument("--cp4ludmila", help="Periodically check if new files need to be copied to transfer dir", action="store_true")
_ap.add_argument("-f", help="Force cp4ludmila to check for every file regardless of ignore_transferred setting", action="store_true")

_pwr_grp = _ap.add_argument_group("power_down", "Commands to power down RDI ADCP over serial port")
_pwr_grp.add_argument("--power_down", help="Send break to ADCP and power it down (cz command)", action="store_true")
_pwr_grp.add_argument("-p", help="Serial Port name in COMnn format")
_pwr_grp.add_argument("-b", help="The baudrate for the ADCP serial port", type=int)

_mem_grp = _ap.add_argument_group("memory_check", "Command for checking memory usage of app")
_mem_grp.add_argument("--mem_check", help="Use this switch to check if the memory usage of an app is within bounds", action="store_true")
_mem_grp.add_argument("--app", help="The app to check", default="WinRiver II", type=str)
_mem_grp.add_argument("--memsize", help="Maximum memory in MB", default=100, type=int)
_mem_grp.add_argument("--restart_acquisition", help="Restart the data acquisition", default="store_false", type=bool)
_mem_grp.add_argument("--memkey", help="Memory pool name, defaults to WorkingSetSize", default="WorkingSetSize")

_ap.add_argument("--pd0_to_geojson", 
                 help="Periodically check for new PD0 files and convert them to geoJSON files for LP dashboard",
                 action="store_true")

args = _ap.parse_args()

# set up the logger
_fmt = logging.Formatter("{levelname:9} {asctime:24} {name} >>> {message}", style="{")
logger.setLevel(logging.DEBUG)

if HAS_CONSOLE:
    _stream_handler = logging.StreamHandler(sys.stdout)
    _stream_handler.setLevel(logging.INFO)
    _stream_handler.setFormatter(_fmt)
    logger.addHandler(_stream_handler)

if args.debug:
    _stream_handler.setLevel(logging.DEBUG)

if args.show_cmd:
    libaa.change_file_handler(logger, _log_path, _fmt, name = "show_cmd")
    libaa.show_cmd()

if args.startwr2:
    libaa.change_file_handler(logger, _log_path, _fmt, name = "startwr2")
    libaa.start_up(_cfg["WR2"]["projectBasePath"], _cfg["WR2"]["mmtSourcePath"], _cfg["WR2"]["wspSourcePath"])

if args.clean_pdir:
    libaa.change_file_handler(logger, _log_path, _fmt, name="clean_pdir")
    pydcp_basepath = pathlib.Path(_cfg["geojson"]["pydcp2ProcessingPath"])
    libaa.clean_processing_dir(
        pydcp_basepath / "documentation\processed_files.txt",
        pydcp_basepath,
        pathlib.Path(_cfg["WR2"]["projectBasePath"]),
        pydcp_basepath / _cfg["geojson"]["pydcp2ncGlobalAttrFile"],
        _cfg["geojson"]["pydcp2ncGlobalSrc"]
    )

if args.pd0_to_geojson:
    libaa.change_file_handler(logger, _log_path, _fmt, name="pd0_to_geojson2")
    pydcp_basepath = pathlib.Path(_cfg["geojson"]["pydcp2ProcessingPath"])
    tracking_file = pydcp_basepath / "documentation\processed_files.txt"
    nc_global_attr_file = pydcp_basepath / _cfg["geojson"]["pydcp2ncGlobalAttrFile"]
    libaa.PD0_to_geojson(
        tracking_file,
        pydcp_basepath,
        pathlib.Path(_cfg["WR2"]["projectBasePath"]),
        pathlib.Path(_cfg["geojson"]["LudmilageojsonTargetPath"])
    )

if args.shutdown:
    libaa.change_file_handler(logger, _log_path, _fmt, name="shutdown")
    libaa.quit_wr2(_cfg["WR2"]["projectBasePath"], _cfg["WR2"]["fileTransferTargetPath"], _cfg["WR2"]["LudmilaFileTransferTargetPath"])

if args.power_down:
    libaa.change_file_handler(logger, _log_path, _fmt, name="power_down")
    libaa.shutdown_RDI_ADCP(args.p, args.b)

if args.cp4ludmila:
    libaa.change_file_handler(logger, _log_path, _fmt, name="copy4ludmila")
    if args.f:
        force = True
    else:
        force = False
    
    libaa.copy_files2transfer_dir(
        _cfg["WR2"]["projectBasePath"],
        _cfg["WR2"]["LudmilaFileTransferTargetPath"],
        force=force
    )

if args.mem_check:
    if HAS_CONSOLE:
        print(f"Checking memory consumption of app {args.app}")

    libaa.change_file_handler(logger, _log_path, _fmt, name="mem_guard")
    _max_mem = int(args.memsize * 1E6)
    if not libaa.is_app_memory_less( appname=args.app, max_memory=_max_mem, memkey=args.memkey):
        if args.restart_acquisition:
            libaa.restart_data_acquisition()
        else:
            logger.warning("Memory usage is out of limits, data acqusition is NOT restartet!")
    
    if HAS_CONSOLE:
        print("This windows will close soon")
        sys.stdout.flush()
        time.sleep(10)