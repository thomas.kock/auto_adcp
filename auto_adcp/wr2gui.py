import locale
lang_id, encoding = locale.getlocale()

if lang_id == "English_United States" or lang_id == "en_US":
    WND_MAIN = "WinRiver II - Teledyne RD Instruments"
    WND_SETCLOCK = "Set ADCP Clock Dialog"
    WND_CLOCK = "ADCP Clock"
    WND_OPENWSP = "Open Workspace File"
    WND_OPENMMT = "Select Measurement File"
    WND_STARTTRANSECT = "Start Transect"
    WND_ENDTRANSECT = "End Transect"

elif lang_id == "de_DE":
    WND_MAIN = "WinRiver II - Teledyne RD Instruments"
    WND_SETCLOCK = "Setze ADCP-Uhrzeit"
    WND_CLOCK = "ADCP-Uhr"
    WND_OPENWSP = "Arbeitsbereich-Datei"
    WND_OPENMMT = "Messungsdatei"
    WND_STARTTRANSECT = "Starte Transekt"
    WND_ENDTRANSECT = "Ende Transekte"