"""
Script used during development on a specific machine to provide NMEA0183 telegrams to 2
VCPs to simulate the presence of GNSS data. Necessary for Winriver2 to obtain a timestamp
from $GPZDA telegram.

Please note: This script will not work without modifications on other machines

thomas.kock@hereon.de
12/2022
"""

import serial
import pathlib
import os
import time

def main(rate: float = 0.5):
    path = pathlib.Path(os.path.expanduser("~"), "Documents", "auto_adcp", "resource", "nmea_samples.nmea")
    with (
        open(path, "r", encoding="utf-8") as nmea_telegrams,
        serial.Serial(port="COM50", baudrate=19200, write_timeout=0) as slink0,
        serial.Serial(port="COM60", baudrate=19200, write_timeout=0) as slink1,
    ):
        t0 = time.perf_counter()
        for nt in nmea_telegrams:
            ntr = (nt.strip("\n") + "\r\n")
            slink0.write(ntr.encode())
            slink0.flushOutput()
            slink1.write(ntr.encode())
            slink1.flushOutput()
            print(f"{ntr}", end="")
            if "RMC" in nt:
                t1 = time.perf_counter()
                time.sleep(rate - (t1 - t0))
                t0 = time.perf_counter()
        
if __name__ == "__main__":
    try:
        while True:
            main()
    except KeyboardInterrupt:
        print("Exit on CTRL-C")
        _ = input("Press return to close window")