﻿# This script is supposed to run on log off on LPs ADCP PC to ensure copy of all collected data files
# To enable this action link the script to powershel-scripts run at user log off using gpedit.msc

Start-Sleep -s 30
Set-Location $HOME\Documents\auto_adcp
& $HOME\Documents\pyenv\Scripts\python.exe -m auto_adcp --cp4ludmila --debug