# I used this script to log the memory consumption of Winriver2 during
# development of Auto ADCP. I am leaving it in the repo for possible future use

set-location $HOME\Documents
$file_datetime = Get-Date -format "yyyyMMddThhmmss"
"Datetime,NPM,PM,WS,CPUt,Handles" | Out-File "mem_${file_datetime}.txt"

while ($proc = get-process winriver*) {
    $current_date = Get-Date -format "yyyyMMddTHHmmss"
    $msg = (
        $current_date + "," + 
        [int]($proc.NPM / 1024) + "," +
        [int]($proc.PM / 1024) + "," +
        [int]($proc.WS / 1024) + "," +
        $proc.CPU + "," +
        $proc.HANDLES
        )
    write-host $msg 
    $msg | Out-File "mem_${file_datetime}.txt" -append
    start-sleep(10)
    }