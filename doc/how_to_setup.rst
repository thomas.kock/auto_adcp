How to set up auto ADCP
#######################

Currently there is no installation or setup script and it is likely that there will never be one.
Therefore we provide this step by step instruction about setting up the individual components of
auto ADCP.

Setting up the host PC
**********************

The host PC is the PC running the data acquisition software WinRiver2 from Teledyne RD Instruments.
All necessary peripherals connect to this PC. Auto ADCP scripts run on this PC as well. In order
to recreate a setup similar to the one we developed and tested auto ADCP against please make sure
to meet the following specs:

- Quad Core CPU at 2.4 GHz or higher
- 16 GB RAM
- At least 3 free serial ports capable of real-time, delay free serial comms via RS-232
- Windows 10 22H2
- WinRiver2 version 2.16
- python 3.11 or higher

.. note:: 

    We use WinRiver2.16 because it turns out to be the most reliable version to date for this
    application. Newer versions need adaptions in the lib_auto_adcp.py code. It is not yet
    known if and when an upgrade might happen

.. note:: 

    We strongly recommend to create a virtual environment for python3 to deploy auto ADCP

To set up a python virtual environment open Powershell and type the following commands. Note that 
the virtual environment will be created in the current working directory, so change it before
if so desired.

.. code-block:: 

    python3.exe -m venv <name of virtual env>

To use the virtual environment you have to activate it, so in the same shell type

.. code-block:: 

    . ./<name of virtuel env>/scripts/activate.ps1

Note that the execution policy on your machine must allow execution of local scripts. If the activation
was successful you will notice a *(<name of virtuel env>)* in your shell. Go ahead now an install
needed python dependencies via *pip*

.. figure:: img/venv.jpg

    Activation of the virtual environment *py311* in powershell

.. code-block:: 

    pip install pywin32 pyserial

The last things to do is to clone the auto_adcp git repository into a directory of your choice.
Note that only cloning using https is supported for anonymous users.

.. code-block:: 

    git clone https://codebase.helmholtz.cloud/thomas.kock/auto_adcp.git

If git is not available on the host PC, cloning is also possible using https from
the browser at `codebase.hereon.de <https://codebase.helmholtz.cloud/thomas.kock/auto_adcp.git>`_

In order to provide all currently available features auto ADCP needs another package called
*pydcp2*, which cannot be provided at the moment. Since the features relying on *pydcp2* are 
highly experimental we will not distribute it.

If all steps have been executed successfully the host PC should be set up correctly to use auto ADCP.

Setting up Winriver2
********************

After installation, there are some settings to make in Winriver 2 in order to make Auto ADCP work 
properly. Within Winriver2 navigate to File -> Properties or press F7. Inside this dialogue navigate
to properties -> general configuration and uncheck all check boxes except *load default workspace layout*.

.. figure:: img/wr2_settings.png

    Settings to make inside Winriver2 for Auto ADCP

The auto_adcp_config.ini file
*****************************

In the main directory of the auto_adcp repository under resources you find the auto\_adcp\config.ini
file, which is used by auto ADCP to set and store settings. Here is an example of the entries.
You may and should change the entires according to your needs. Lines with an asterisk (*) are 
used only when real time processing is enabled. 

| [SerialPorts]
| ADCP = COM13
| ADCPBaud = 57600
| GPS = COM12
| GPSBaud = 19200
| HDT = COM14
| HDTBaud = 19200

| [Logging]
| logPath = C:\\Users\\ADCP\\Documents\\log_auto_adcp

| [WR2]
| projectBasePath = E:\\adcp_prandtl_WRII\\wr2_1200khz_labconfig_basin1
| mmtSourcePath = C:\\Users\\ADCP\\Documents\\auto_adcp\\resource\\wr2_1200khz_labconfig_basin1.mmt
| wspSourcePath = C:\\Users\\ADCP\\Documents\\auto_adcp\\resource\\wr2_1200khz_labconfig_basin1.wsp
| fileTransferTargetPath = E:\\temp_e\\wr2_data
| LudmilaFileTransferTargetPath = E:\\adcp_prandtl

| [AutoADCP]
| ignoreTransferredFiles = 1
| transferredFileID = S\_

| [geojson]
| * pydcp2ProcessingPath = E:\\adcp_prandtl_WRII\\processing
| * pydcp2ncGlobalAttrFile = documentation\\nc_global.json
| * pydcp2ncGlobalSrc = C:\\Users\\ADCP\\Documents\\auto_adcp\\resource\\nc_global.json
| * LudmilageojsonTargetPath = E:\\adcp_prandtl_geojson
| * pd0PollingInterval = 60
| * pydcp2CleanUp = 1

Setting up the start up option
******************************

All auto ADCP scripts are python scripts relying on the the built in Windows Task Scheduler for
automated execution. Therefor tasks have to be configured. To simplify this process auto ADCP
comes with a bunch of python scripts to set the tasks up in Windows Task Scheduler. They are
located in /auto_adcp/task_scheduler and should be called from there. In the task_scheduler 
directory you find another .ini file to make the settings for the tasks. Have a look into the 
setup_tasks.ini file for detailed explanations of the options and properties.

All tasks for auto ADCP will be placed in a subfolder called Auto ADCP by default inside the 
Task Libary in Task Scheduler. The task setup script rely on the pywin32 package once again,
so be advised to run the script with your previously set up virtual environment (venv).

To check the tasks open windows search, search for **task scheduler** (Aufgabenplanung in german)
and open the task library / Auto ADCP. Inside you should see several planned tasks you can explore.

To initially set the task or or update their settings after changing setup_tasks.ini call

.. code-block::

    (venv) PS> python /path/to/auto_adcp/task_scheduler/setup_auto_adcp_tasks.py

Deleting all tasks is done similarly

.. code-block::

    (venv) PS> python /path/to/auto_adcp/task_scheduler/delete_auto_adcp_tasks.py

A third script is included to temporarily disable the planned tasks. This may be convenient if 
automatic data acquisition has to be disabled for some reason. Note that the disabled tasks are 
reenabled immediately, thus running normal after next user logon, which is the main trigger for 
all tasks.

.. code-block::

    (venv) PS> python /path/to/auto_adcp/task_scheduler/disable_auto_adcp_tasks.py

.. figure:: img/taskscheduler.jpg

    The tasks set up in Windows Task Scheduler

Actions on shutdown
*******************

In out set up the host PC is shutdown by remotely shortening the power button. This means there 
no clean exit from Winriver 2 possible. This is an edge case, that can - to my knowledge - not 
be handled with the task manager, as there is no such trigger like "on shutdown down". It is 
however possible to add a powershell script that is executed when the user is logged out via 
gpedit.msc. 

For our installation a last file copy action is needed after requesting shut down. In 
auto_adcp/resources you find the copy_on_logoff.ps1 script. To use is open Windows search, type
**gpedit.msc** and open the group policy editor. Navigate to User configuration -> Windows settings 
-> scripts -> logoff -> powershell-scripts and add copy_on_logoff.ps1 there.

.. figure:: img/gpedit.png

    The group policy editor and where to find the log off scripts

.. figure:: img/psscript.png

    Add the desired script to run at log off under the powershell-scripts tab

A final word on stability
*************************

The module functions of auto_adcp and the scripts combining them are tested in many long
running test with both virtual instrument as well as on real hardware. We find auto_adcp
to be stable.

**BUT**

Auto ADCP has no notion of the current state of the WinRiver2 software and from our experience
WinRiver2 is rather unstable, especially for longer runs. On our vessel this may be a problem as 
they are collecting data for an absolute maximum of 12 h.

WinRiver2 has a memory problem and it will eventually crash after several hours. Observations
during tests and cruises indicate that this crash happens sometimes after only 7 hours. 

To counter this problem auto ADCP offers a memory guard function, that will check the current 
working set size of the Winriver2 app and if working set size exceeds a certion user defined 
threshold auto ADCP will restart data collection by issuing the virtual keys to stop transect, 
stop pinging, start pinging, start transect. If this is done Winriver2 flushes its working set 
and can collect data for many more hours reliably according to our longterm tests.

A typical config for auto ADCPs mem_check would be something like this. This is also the default
in setup_tasks.ini. The mem_check task is set up to run every 1 h by default.

.. code-block::

    python.exe --debug --mem_check --memsize 512

If Winriver2 crashes, Auto ADCP processes still running will continue to run and report logs. 
But without WinRiver2 there will be no more data.
