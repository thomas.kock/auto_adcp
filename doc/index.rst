.. Auto ADCP documentation master file, created by
   sphinx-quickstart on Tue Jul  4 06:33:59 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Auto ADCP's documentation!
=====================================

auto_adcp is a collection of automation tools for Teledyne RDIs WinRiver2 (WR2) ADCP data 
acquisition software. It has been developed to enable "turnkey" operation after PC power on, 
much like it was available "builtin" by default in WinRiver1. auto_adcp has been developed at 
Helmholtz-Zentrum Hereon for use on Hereons Research Vessels Ludwig Prandtl and Coriolis. It is 
somewhat tailor made for this setup, but may be modified and adapted to other platforms. 
auto_adcp does not provide an installer, settings are made in many different places, see 
detailed setup information below.

.. figure:: img/winriver2.jpg
   
   TRDIs Winriver 2 software for ADCP data collection

As second step auto_adcp can read, parse and coordinate transform collected ADCP data directly
out of TRDIs PD0 format to provide near-real-time (NRT) data for geo web applications such as
ESRI. The output format is then .geojson.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   functional_description
   how_to_setup
   about_logging
   lib_auto_adcp



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
