Functional Description of auto ADCP
===================================

Auto ADCP is a set of tools to automate data collection with Teledyne RDI (TRDI) ADCPs running 
TRDIs Data Acquisition software WinRiver 2. Thus it should be considered as a wrapper around
TRDIs software. 

Together with another toolbox developed at Helmholtz-Zentrum Hereon called **pydcp2** auto ADCP
enables automatic processing of the collected raw data files (TRDIs binary .PD0 format). To 
achieve this auto ADCP can be called with different options to enable certain functionality. 

Auto ADCP may be used without a local deployment of **pydc2**, then without the processing
capabilities. **pydcp2** is under heavy development at hereon and therefore not yet publicly 
available. If you want to use it you may inquire at thomas.kock(at)hereon.de.

.. note:: 

    pydcp is not shipped with auto_adcp. Except for the -\-pd0_to_geojson option (see below) 
    auto_adcp will run without it. 

Auto ADCP is written in python. To start scripts using auto ADCP use one of

.. code-block::

    python.exe -m auto_adcp <options>
    pythonw.exe -m auto_adcp <options>

Use the pythonw option if auto ADCP is supposed to run a a background task with no visible
window opening. Documented options include but are not limited to

================= =============================
Option            Description
================= =============================
-\-startwr2       Launch WinRiver2 and start data collection
-\-clean_pdir     Should be called before every new data collection if auto processing is used
-\-pd0_to_geojson Process PD0 files and output geoJSON files for use in geo browsers
-\-power_down     Shutdown the ADCP sending a break and CZ command
-\-shutdown       Can be called after requesting shutdown of the OS to copy the last collected files
-\-show_cmd       Try to always show the cmd window as the topmost window
-\-cp4ludmila     Periodically check for new data files and copy them to LUDMILA transfer directory
-\-mem_check      Used to work around a memory leak present in Winriver2.16
================= =============================
