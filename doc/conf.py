# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import sphinx_rtd_theme
import sys, os
sys.path.insert(0, os.path.abspath('..'))
add_module_names = False
from auto_adcp import lib_auto_adcp

project = 'Auto ADCP'
copyright = '2024, Helmholtz-Zentrum Hereon'
author = 'Thomas Kock'
release = f'{lib_auto_adcp.__version_major__}.{lib_auto_adcp.__version_minor__}'
version = release

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_rtd_theme"
    ]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
autodoc_mock_imports = [
    "win32gui", 
    "win32com.client", 
    "win32con", 
    "serial",
    "pydcp2",
    "trdi",
    "pydcp2.io",
    "pydcp2.processing",
    "pydcp2.geo",
    "pydcp2.tools",
]



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
