About Logging
#############

By default auto_adcp logs everything (i.e. level=DEBUG) to log files located in a
directory set in the auto_adcp_config.ini file. Each call will produce a new logfile with
the call option - for instance --startwr2 - and a timestamp in the filename.

There is an additional stream_handler set up to direct less verbose logging output to
stdout. This can be read in shell windows if auto_adcp is called with python.exe. 

If you call auto_adcp with pythonw.exe, no stdout is present and therefore no output is 
shown. In fact, some scripts may even report crashes if called with pythonw.exe if they 
attempt to write to stdout or stderr.